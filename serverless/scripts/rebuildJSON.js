var jsonfile = require('jsonfile')
var file = process.argv[2]
jsonfile.readFile(file, function(err, obj) {
    if (err) {
        console.error(err);
    } else {
        var result = JSON.parse(JSON.stringify(obj, function(key, value) {
          if (key === 'options') {
            return undefined;
          }
          else if (key === 'default' && value[0] === '/') {
            return value.substring(1);
          }
          else if (key === 'title') {
            return process.env.CI_PROJECT_NAME;
          }
          return value
        }));
        jsonfile.writeFile(file, result, function(err) {
            err ? console.error(err) : console.log(`${file} exported successfully.`);
        })
    }
})