#!/bin/bash
docURL=https://app.swaggerhub.com/apis-docs/$SWAGGER_USER/$SWAGGER_PROJECT_NAME/$STAGE
filename=openapi.json
printf "\nPublishing new Swagger Doc Version
  Stage: $STAGE
  User: $user
  File Name: $filename\n\n
  "

curl -X POST "https://api.swaggerhub.com/apis/$SWAGGER_USER/$SWAGGER_PROJECT_NAME?isPrivate=false&version=$STAGE&oas=3.0.1" \
    -H "accept: application/json" -H "Authorization: $SWAGGER_API_KEY" -H "Content-Type: application/json" \
    -d @$CI_PROJECT_DIR/$filename

printf "
Done, your Swagger UI is published
You can view it at $docURL\n\n"