#!/usr/bin/env bash
# Downloads the JSON OpenAPI document
cd `dirname $0`
set -e
apiId=`bash -c "aws apigateway get-rest-apis --output=json --region=$AWS_DEFAULT_REGION | /usr/bin/env node ./extract-rest-api-id.js $STAGE"`
outputFileNameOAS30=openapi.json

printf "\nDownloading OpenAPI 3.0 definition to $CI_PROJECT_DIR/$outputFileNameOAS30
  API ID: $apiId
  Stage: $STAGE
  OpenAPI version: 3.0
  Accept: $fileType\n\n"

aws apigateway get-export \
  --rest-api-id=$apiId \
  --stage-name=$STAGE \
  --export-type=oas30 \
  --accept=application/$fileType \
  --region=$AWS_DEFAULT_REGION \
  $outputFileNameOAS30

bash -c "node ./rebuildJSON.js $CI_PROJECT_DIR/$outputFileNameOAS30"

printf "
Done, your OpenAPI document is: $CI_PROJECT_DIR/$outputFileNameOAS30
Go to http://editor.swagger.io/ and paste the contents of your OpenAPI document to see it in Swagger UI\n"