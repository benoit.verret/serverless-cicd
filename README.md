# GitLab CI

Explication de l'expérimentation d'un pipeline de déploiement avec GitLab CI.
L'utilisation de Serverless et de VueJS est présente uniquement pour la démonstration,
les concepts peuvent être appliqués à n'importe quel type d'application.

## Sections

- [`.gitlab-ci.yml`](##fichier-gitlab-ciyml`)
- [`Serverless-*.gitlab-ci.yml`](docs/Serverless.md)
- [`VueJS-*.gitlab-ci.yml`](docs/VueJS.md)
- [`Swagger|OpenAPI-*.gitlab-ci.yml`](docs/Swagger-OpenAPI.md)
- [`CloudFront-*.gitlab-ci.yml`](docs/CloudFront.md)

## Fichier `.gitlab-ci.yml`

Dans le fichier `.gitlab-ci.yml` situé à la racine du projet, nous avons 5 parties.

### Stages

La clef `stages` sert a définir les étapes de notre pipeline, certaines étapes sont utilisé par la partie Back End uniquement, d'autre pour le Front End et certaine par les 2.

Des étapes peuvent aussi être utilisées pour l'exécution de commande autre telles que l'invalidation de la cache CloudFront.

```yml
stages:
  - pre_build
  - build
  - test:unit
  - test:e2e
  - deploy
  - generate_doc
  - publish_doc
  - invalidation
```

### Variables

Le fait de définir les variables à l'extérieur d'un `Jobs` rend ces variables accessibles dans toutes les `Jobs`.
On peut simplifier en disant que les variables sont déclarées *globalement*.

```yml
variables:
  SERVERLESS_DIR: serverless
  HTML_DIR: html
  SERVERLESS_IMAGE: $CI_REGISTRY_IMAGE/
  HTML_IMAGE: $CI_REGISTRY_IMAGE/
  CLOUDFRONT_ID: ""
```

Il est important de comprendre que de redéfinir le mot clef `variables` ou tout autre mot clef à l'intérieur d'une `Job`, donc localement, ne concatène pas les variables globales et locales, mais remplace tout ce qui est présent dans `variables` pour la `Job` qui les redéfinis.

```yml
variables:
  VAR1: 'patate'
  VAR2: 'steak'

job1:
  ...
  script:
    - echo $VAR1 # Affiche "patate"

job2:
  ...
  variables:
    - VAR2: 'frite'
  script:
    - echo $VAR1 # $VAR1 n'existe pas
    - echo $VAR2 # Affiche "frite"

job3:
  ...
  script:
    - echo $VAR1 # Affiche "patate"
    - echo $VAR2 # Affiche "steak"
```

### Include

Le mot clef `include` permet d'ajouter des actions GitLab CI provenant de ressources externes.
Les fichiers peuvent être présent à même le repo du projet et être ciblés grâce à la clef `local`, mais peuvent aussi provenir d'un autre repo, d'un URL public ou de template officiel de GitLab, [plus d'information ici](https://docs.gitlab.com/ee/ci/yaml/#include).

Importation des Jobs définis localement. L'ordre peut être important, car la dernière pourrait remplacer des éléments ayant le même nom.

```yml
include:
  - local: /gitlab-ci/Jobs/Serverless-Build.gitlab-ci.yml
  - local: /gitlab-ci/Jobs/VueJS-Build.gitlab-ci.yml
  # - local: /gitlab-ci/Jobs/VueJS-Test.gitlab-ci.yml
  - local: /gitlab-ci/Jobs/OpenAPI-Generate.gitlab-ci.yml
  - local: /gitlab-ci/Jobs/Serverless-Deploy.gitlab-ci.yml
  - local: /gitlab-ci/Jobs/VueJS-Deploy.gitlab-ci.yml
  - local: /gitlab-ci/Jobs/Swagger-Publish.gitlab-ci.yml
  - local: /gitlab-ci/Jobs/CloudFront-Invalidation.gitlab-ci.yml
```

En dehors des Jobs, on pourrait aussi imaginer avoir nos variables à l'extérieur.

### Before Script

La clef `before_script` définie du script a exécuté avant les scripts "normal" et pour toutes les `Jobs`, son contenu sera donc réexécuté avant le script de chaque `Job`.

```yml
before_script:
  - *auto_devops
  - set_registry_image_variables
  - set_stage_variable
```

On peut voir ici l'utilisation de la syntaxe spéciale **YAML** `*auto_devops` qui permet d'inclure le contenu de l'ancre `&auto_devops`.

### Utilisation d'ancre et définition de fonction

*Noter que le nom `.auto_devops` est arbitraire.*

J'ai défini plusieurs fonctions que je peux réutiliser au travers des `Jobs` selon les besoins.

Par exemple, `registry_login` nous log au registry de GitLab, mais on aurait aussi pu mettre une condition du type **« Si la variable AWS_ECR est définie, se connecter au Registry d'AWS ECR »**.

```sh
  function registry_login() {
    if [[ -n "$CI_REGISTRY_USER" ]]; then
      echo "Logging to GitLab Container Registry with CI credentials..."
      echo $CI_REGISTRY_PASSWORD | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
      echo ""
    fi
  }
```

La fonction `set_registry_image_variables` permet de définir mes noms d'images pour de mon Front End et Back End, mon projet contient les 2, je veux avoir le même script pour créer ou mettre à jour les images et définir leur nom dépendamment si le projet à plusieurs images a créé. À noter que si on utilise AWS ECR on voudra peut-être ajouter une condition ou un argument pour remplacer `$CI_REGISTRY_IMAGE`.

```sh
  function set_registry_image_variables() {
    echo "Set image source dynamically..."
    if [[ -n "$SERVERLESS_DIR" ]]; then
      echo "Serverless directory have been found, setting image name..."
      export SERVERLESS_IMAGE="$CI_REGISTRY_IMAGE/$SERVERLESS_DIR"
    fi
    echo "Serverless Image name set to $SERVERLESS_IMAGE"
    if [[ -n "$HTML_DIR" ]]; then
      echo "HTML directory have been found, setting image name..."
      export HTML_IMAGE="$CI_REGISTRY_IMAGE/$HTML_DIR"
    fi
    echo "HTML Image name set to $HTML_IMAGE"
    echo ""
  }
```

La fonction `set_cloudfront_id` permet de configurer globalement la variable `CLOUDFRONT_ID`, elle prend en argument l'ID à définir. **VueJS** et le plugin **S3 Deploy** déploient le tout selon des variables d'environnement présent dans des fichiers `.env.production`, `.env.staging`. Cette fonction me permet donc de définir la variable globalement pour une prochaine `Job` selon l'environnement de déploiement. Dans mon cas je m'en sers pour la `Job` d'invalidation de la cache **CloudFront**.

```sh
  function set_cloudfront_id() {
    if [[ -n "$1" ]]; then
      echo "Set CloudFront ID to $1..."
      export CLOUDFRONT_ID="$1"
      echo ""
    fi
  }
```

La fonction `invalidate_cloudfront` prend en argument une liste de paths, à défaut d'en avoir elle invalidera toute la cache. Elle exécute ensuite une commande `aws-cli` pour lancer l'invalidation. Bien sur pour fonctionner elle doit être appeler dans un container ayant aws-cli d'installer et les variables `AWS_ACCESS_KEY_ID`, `AWS_DEFAULT_REGION` et `AWS_SECRET_ACCESS_KEY` doivent être configurer dans GitLab.

```sh
  function invalidate_cloudfront() {
    paths="/"
    if [[ -n "$1" ]]; then
      paths=$1
    fi
    if [[ -n "$CLOUDFRONT_ID" ]]; then
      echo "Invalidating CloudFront cache from ID $CLOUDFRONT_ID..."
      aws cloudfront create-invalidation --distribution-id=$CLOUDFRONT_ID --paths=$paths
      echo ""
    fi
  }
```

Un peut comme pour `set_registry_image_variables`, ces deux fonctions servent à définir la racine de mon Front End et Back End pour exécuter les commandes au bon endroit dans mes `Job`.

```sh
  function serverless_set_directory() {
    if [[ -n "$SERVERLESS_DIR" ]]; then
      echo "Move to serverless project directory..."
      cd $SERVERLESS_DIR
      echo ""
    fi
  }
  function html_set_directory() {
    if [[ -n "$HTML_DIR" ]]; then
      echo "Move to HTML project directory..."
      cd $HTML_DIR
      echo ""
    fi
  }
```

La fonction `build_docker` permet de construire l'image, elle va récupérer la plus récente si elle existe et l'utiliser comme cache pour en construire une nouvelle avec les modifications appliquées. C'est en suivant l'exemple de GitLab que j'ai choisi de construire 2 images, un avec le tag latest, l'autre avec le hash du commit comme tag, cette méthode permet de garder un suivi des versions. Une fois construit, les images seront poussées vers le registry du projet de GitLab. Si mon projet à plusieurs parties (Front End et Back End) j'ajoute l'option d'inclure en argument un string qui sera concaténé au nom de mon image, dans mes Jobs j'utilise entre autres `build_docker $SERVERLESS_DIR`.

```sh
  function build_docker() {
    image_name=$CI_REGISTRY_IMAGE
    echo "Set registry image name to build..."
    if [[ -n $1 ]]; then
      image_name="$CI_REGISTRY_IMAGE/$1"
    fi
    echo "Image name set to $image_name"
    echo ""
    echo "Pull image from registry to by used as cache..."
    docker pull --quiet $image_name$folder:latest || true
    echo ""
    echo "Build image from pulled image cache and create `latest` and `commit_sha` tags..."
    docker build --quiet --cache-from $image_name$folder:latest --tag $image_name$folder:$CI_COMMIT_SHA --tag $image_name$folder:latest .
    echo ""
    echo "Push the tagged Docker images to the container registry.."
    docker push $image_name$folder:$CI_COMMIT_SHA
    docker push $image_name$folder:latest
    echo ""
  }
```

Finalement, la fonction `set_stage_variable` me permet de définir ma variable `$STAGE` selon ma branche, l'exemple ici indique que pour la branche `master` mon stage est `staging` et que pour la branche `production` il utilisera `production`. La variable `$STAGE` est ensuite passé lors des build/deploy tel que `yarn build --mode=$STAGE` ou `sls deploy --stage=$STAGE`.

```sh
  function set_stage_variable() {
    echo "Set current stage..."
    if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then
      export STAGE="staging"
    elif [[ "$CI_COMMIT_REF_NAME" == "production" ]]; then
      export STAGE="production"
    fi
    echo "Stage set to `$STAGE`"
    echo ""
  }
```
