const path = require("path");

module.exports = {
  devServer: {
    disableHostCheck: true,
    port: 8080,
    https: false,
    liveReload: true,
    hot: true,
    host: "0.0.0.0",
    watchOptions: {
      ignored: /node_modules/,
      aggregateTimeout: 300,
      poll: true
    }
  },

  configureWebpack: {
    resolve: {
      alias: {
        src: path.resolve("src"),
        api: path.resolve("src/api"),
        auth: path.resolve("src/auth"),
        sass: path.resolve("src/sass"),
        utils: path.resolve("src/utils"),
        store: path.resolve("src/store"),
        views: path.resolve("src/views"),
        router: path.resolve("src/router"),
        types: path.resolve("src/typings"),
        components: path.resolve("src/components")
      }
    }
  },

  
  // Deactivation of sourceMap in production
  productionSourceMap: process.env.NODE_ENV !== 'production',

  pluginOptions: {
    s3Deploy: {
      registry: undefined,
      awsProfile: "default",
      region: `${process.env.VUE_APP_REGION}`,
      bucket: `${process.env.VUE_APP_MODE}-HelloWorld-html`,
      createBucket: true,
      staticHosting: true,
      staticIndexPage: "index.html",
      staticErrorPage: "index.html",
      assetPath: "dist",
      assetMatch: "**",
      deployPath: "/",
      acl: "public-read",
      pwa: true,
      pwaFiles: 'service-worker.js',
      enableCloudfront: true,
      cloudfrontMatchers: "/*",
      uploadConcurrency: 5,
      pluginVersion: "3.0.0"
    }
  },
  // Deactivation of sourceMap in production
  productionSourceMap: process.env.NODE_ENV !== "production",

  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "src/sass/_shared.scss";'
      }
    }
  },

  chainWebpack(config) {
    const FILE_RE = /\.(vue|js|ts|svg)$/;

    config.module
      .rule("svg")
      .issuer(file => !FILE_RE.test(file))
      .oneOf("svg");

    config.module
      .rule("svg-component")
      .test(/\.svg$/)
      .issuer(file => FILE_RE.test(file))
      .oneOf("ignore")
      .resourceQuery(/\?ignore/)
      .use("file-loader")
      .loader("file-loader")
      .end()
      .end()
      .oneOf("normal")
      .use("vue")
      .loader("vue-loader")
      .end()
      .use("svg-to-vue-component")
      .loader("svg-to-vue-component/loader");
  }
};
