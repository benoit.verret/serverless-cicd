import Vue from "vue";
import VueRouter, { NavigationGuard, Route, RouteConfig } from "vue-router";
import lazyLoading from "./lazyLoading";
import Home from "../views/Home.vue";

type Next = Parameters<NavigationGuard>[2] | any;

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    component: lazyLoading("About"),
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to: Route, from: Route, next: Next) => {
  const title: string = to.meta.title || "HelloWorld App!";
  document.title = title;
  next();
});

// router.afterEach(() => {});

export default router;
