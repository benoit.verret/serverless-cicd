import Vue from "vue";
import Vuex from "vuex";
import modules from './modules';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';
import state from './mutations';

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules
});
