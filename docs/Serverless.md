# Jobs Serverless

Notez que l'utilisation de Serverless pour la démonstration est arbitraire, c'est relativement générique, basé sur docker et adaptable à toute technologie.

## Build

Considérant qu'une application **Serverless** n'a rien en soit à builder, cette étape est utilisée pour générer l'image docker contenant les ressources nécessaires.

Dans le fichier `Serverless-Build.gitlab-ci.yml` se trouve la définition de la `Job` à exécuter, le nom de la job, soit `serverless:build` est arbitraire, mais il est courant d'utilisé cette syntaxe pour bien identifier nos jobs, on pourrait avoir `nom_client_ou_projet:techno:etape_du_pipeline`.

### Définition de l'image et service.

En simple, on dit quelle image utiliser avec quel tag. Si l'ensemble des `Jobs` utilise la même image, on priorisera plus la définition de l'image à même la racine.

Dans une job:
```yml
serverless:build:
  image: docker:19.03.11
  services:
    - docker:19.03.11-dind
```

À même la racine:
```yml
image: docker:19.03.11
services:
  - docker:19.03.11-dind

serverless:build:
  script:
  ...
```

Les services permet de définir une variante d'une image, selon GitLab le principal use case concerne les bases de données, on pourrait dire **« Utilise l'image redmine:4.1.1 avec le service redmine:4.1.1-mysql ou redmine:4.1.1-postgresql »**.

### Définition de l'étape du Pipeline

J'indique à quelle étape parmi ceux signifiés dans le fichier `.gitlab-ci.yml`.

```yml
  stage: build
```

### Script

Dans les scripts j'exécute soit des commandes directement, ou sinon des fonctions préalablement définies, dans mon cas dans le `before_script` de mon fichier `.gitlab-ci.yml`.
J'en profite aussi pour ajouter du verbose pour faciliter la compréhension de ce qui sera exécuté lors de la `Job`.

Dans ce Build je me log au registry de GitLab, je définis le chemin vers mon application serverless et je construis mon image en lui passant en argument le nom de mon dossier à concaténer dans le nom de l'image qui sera créé.

```yml
  script:
    - echo "Build serverless image with preinstalled node modules dependency"
    - registry_login
    - serverless_set_directory
    - build_docker $SERVERLESS_DIR
```


### Only

Plusieurs conditions `only` peuvent exister, elle définisse si la job s'exécute ou non, ça peut être selon l'existence de variable, le nom d'une variable, les changements commité.

Dans ce cas, l'image construire sert à déployer une application serverless, l'image contient toutes les ressources nécessaires soit les dépendances définies dans `package.json`.
Pour optimiser le processus, j'utilise donc mon image de base `amaysim/serverless:1.72.0` dans laquelle j'installe mes modules NPM. Il est à noter que le `WORKDIR` d'`amaysim/serverless` est `/opt/app`, il est important de le savoir pour les étapes suivantes.

```dockerfile
FROM amaysim/serverless:1.72.0

COPY package.json ./
RUN yarn
```

Puisque de développer une application serverless ne nécessite pas une nouvelle image à chaque fois qu'on modifie le code d'une fonction lambda, j'ai opté pour activer cette `Job` uniquement s’il y a eu un changement si les dépendances du projet, le Dockerfile ou la `Job` ont été modifiés. On peut donc sauver plusieurs minutes de reconstruction d'image.

```yml
  only:
    changes:
      - $SERVERLESS_DIR/package.json
      - $SERVERLESS_DIR/Dockerfile
      - gitlab-ci/Jobs/Serverless-Build.gitlab-ci.yml
```

## Deploy

Pour le déploiement, j'utilise l'image la plus récente créée. Si l'étape de build a eu lieu, une nouvelle version d'image a été créée, sinon elle utilise la plus récente préalablement créée. Considérant que mon projet contient deux sections, je nomme dynamiquement mon image.

```yml
serverless:deploy:
  image: $SERVERLESS_IMAGE$SERVERLESS_DIR:latest
  stage: deploy
```

### Script

Tout comme à l'étape précédente, je définis l'emplacement de mon projet. 

```yml
  script:
    - serverless_set_directory
```

Pour la suite il faut comprendre que GitLab défini lui-même le working directory, il se nomme `/builds/...`, mais puisque mon image utilisait un path différent, je crée un lien symbolique entre le `WORKDIR` `/opt/app` dans lequel j'ai installé mes dépendances pour les mettre à la racine de mon projet serverless présentement ouvert.

```yml
    - ln -s /opt/app/node_modules ./node_modules
```

Au lieu d'utiliser les commandes `serverless` directement j'ai décidé de les définir dans mon `package.json`. Une bonne pratique pourrait être l'utilisation de Makefile pour standardiser notre travail, peu importe la technologie utilisée.

Les variables d'environnement ayant une portée partout, je peux les utiliser à même mon fichier `package.json`.

Dans ces scripts, j'exporte le output vers un fichier texte pour ensuite l'afficher avec `cat`, l'affichage permet de le visualiser dans GitLab à même l'exécution de la `Job`, alors que le fichier texte sera exporté comme artifact.

```json
  "print": "sls print > $CI_PROJECT_DIR/serverless_print_logs.txt && cat $CI_PROJECT_DIR/serverless_print_logs.txt",
  "deploy": "sls deploy -v --stage=$STAGE > $CI_PROJECT_DIR/serverless_deploy_logs.txt && cat $CI_PROJECT_DIR/serverless_deploy_logs.txt",
```

En exécutant le script yarn print et/ou yarn deploy je me retrouve à exécuter les scripts ci-haut.

```yml
    - yarn print
    - yarn deploy # need to set AWS credentials and region variables in GitLab Secrets
```

### Artifacts

Optionnellement, j'ai décidé d'exporter en artifacts les outputs de certaines commandes. On pourrait imaginer un cas où une étape bâtit un fichier Terraforme ou CloudFormation dynamiquement pour ensuite l'exporter en artifiact et une autre étape qui pourra réutiliser ces artifacts pour déployer les ressources selon les templates générés.

```yml
  artifacts:
    paths:
      - serverless_print_logs.txt
      - serverless_deploy_logs.txt
    expire_in: 1 week
```

### Only, When, Allow Failure

Comme pour le build, je définis les conditions d'exécutions de ma `Job`. Mon application serverless utilisant des fonctions Lambda écrites en Python je demande donc d'exécuter le travail uniquement si j'ai des fichiers `.yml` ou `.py` qui ont été altérés tel que `serverless.yml`, j'inclus aussi le fichier de définition de la `Job` si jamais la mécanique de déploie change.

```yml
  only:
    changes:
      - $SERVERLESS_DIR/**/*.{py,yml}
      - gitlab-ci/Jobs/Serverless-Build.gitlab-ci.yml
```

On pourrait vouloir une approbation avant de déclencher le déploiement, on peut donc spécifier quand exécuter le travail, lorsque configuré sur `manual`, on doit manuellement cliqué sur Start pour démarrer le déploiement. Cette action peut aussi être faite via l'API de GitLab, des mécaniques pourraient donc être jumelées avec le Hub, Slack ou des courriels.

```yml
  when: manual
```

L'échec d'une étape stoppant un Pipeline, on peut définir grâce à ce flag si l'échec est fatal ou si d'autres Job peuvent continuer par la suite.

```yml
  allow_failure: false
```
