# Jobs VueJS

Notez que l'utilisation de VueJS pour la démonstration est arbitraire, c'est relativement générique, basé sur docker et adaptable à toute technologie.

## Build

Dans le build j'ai en fait inclus deux étapes, le `pre_build` qui sert à bâtir l'image utilisée pour les autres étapes et le `build` qui utilise l'image préalablement créée pour build l'application VueJS.

Pour le `pre_build` est quasiment un copier-coller du `build` de Serverless, la différence se situe dans les variables utilisées et le Dockerfile.

Dans le Dockerfile je pars d'un node léger auquel j'ajoute vue-cli que j'utilise pour mon application, je défini le WORKDIR et j'instal les dépendances du projet.

```dockerfile
FROM node:lts-alpine

RUN yarn global add @vue/cli

WORKDIR /opt/app

COPY package.json ./
RUN yarn
```

Pour le build, un peu comme pour le deploy de serverless, je crée un lien symbolique entre les dépendances installées et le dossier de mon projet et j'exécute le script build défini dans `package.json`.

```yml
  script:
    - html_set_directory
    - ln -s /opt/app/node_modules ./node_modules
    - yarn build --mode=$STAGE
```

Finalement, je fais un lien de mon build vers la racine du projet, puisque mon App est dans `./html` le build se retrouve dans `./` directement.

La raison est que les variables d'environnement ne fonctionnent pas dans les paths des artifacts, l'emplacement doit donc être constant et hardcodable.

```yml
    - ln -s $HTML_DIR/dist $CI_PROJECT_DIR/dist
```

Dans mes artifacts j'exporte des logs quelconques, mais surtout mon build que je vais pouvoir utiliser à mon étape de déploiement.

```yml
  artifacts:
    paths:
      - html_build_logs.txt
      - dist/
    expire_in: 5 hrs
```

Finalement, je définis des conditions selon lesquelles je veux que ma `Job` s'exécute.

```yml
  only:
    changes:
      - $HTML_DIR/*
      - gitlab-ci/Jobs/VueJS-Build.gitlab-ci.yml
```

## Deploy

Pour mon déploiement, je demande une exécution manuelle. Encore très répétitif, le point important est le script suivant:

```yml
    - set_cloudfront_id $VUE_APP_S3D_CLOUDFRONT_ID
```

Noter que je fais référence à une variable définir dans `.env.production` et/ou `.env.staging`, il s'agit de l'ID de la distribution CloudFront, j'utilise donc une fonction préalablement définie pour configurer la variable globale CLOUDFRONT_ID qui sera utilisée à l'étape d'invalidation de CloudFront.

Finalement, je spécifie que cette `Job` dépend de la réussit de la job nommée `vuejs:app:build`. Puisque ma job `deploy` utilise un artifact créé dans `build` ce n'était pas nécessaire de le spécifier, il s'agit surtout de montrer l'existence de cette clef.

```yml
  dependencies:
    - vuejs:app:build
```

D'autres clefs permettent aussi d'exécuter certaines `Job` seulement si une étape précédente à échouer, c'est un type d'action intéressant pour des rollback.

## Test

Relativement simple à comprendre, c'était des étapes qui exécutait des tests unitaires, je les ai désactivés, car ils sont longs a exécuté, mais le principe est présent dans le fichier `VueJS-Test.gitlab-ci.yml`.
